#include <iostream>
#include <cstdlib>

using namespace std;

int* generatorProstoy(int n, int beg, int en){
    int* arr = new int(n);

    for(int i = 0; i < n; i++){
        arr[i] = beg+rand()%en;
    }

    return arr;
}

int* generatorUnikalniy(int n, int beg, int en){
    int* mas = new int(n);
    int* mas_pos = new int(en-beg+1);

    int ch = beg;
    for(int i = 0; i < en-beg + 1; i++){
        mas_pos[i] = ch;
        ch++;
    }

    for(int i = 0; i < n; i++){
        int pos = rand()%(en-beg);
        mas[i] = mas_pos[pos];
        mas_pos[pos] = mas_pos[en-beg];
        en--;
    }
    return mas;
}

int main()
{
    int n, start, stop;
    cin >> n >> start >> stop;

    int* mas = generatorProstoy(n, start, stop);
    for(int i = 0; i < n; i++){
        cout << mas[i] << ' ';
    }

    cout << endl;

    int* unikMas = generatorUnikalniy(n, start, stop);

    for(int i = 0; i < n; i++){
        cout << unikMas << ' ';
    }

    return 0;

}
